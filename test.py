import random
import time

readMe = open('file.txt', 'r')
words = [line.strip()for line in readMe]
choose = True
print("\n"*5)
print("Hangman is a paper and pencil guessing game,i will think in a word and you should guess it's letters")
print(",you have 6 triles so do your best ... we are going to start in 5 seconds \n\n")
time.sleep(2)
for i in range(1, 6):
    print(i)
    time.sleep(1)
print("\n"*10)

while choose:

    randNum = random.randint(0,len(words)-1)
    selected_Word = words[randNum]
    gussed_word = ""
    wrongGuess = ""
    guessNumber = 6
    rightAnswer = 0

    for i in range(0, len(selected_Word)):
        gussed_word += "-"

    print(gussed_word)
    while guessNumber != 0 and rightAnswer != len(selected_Word):

        guessedChar = input("your guess (letters only) : ")
        if guessedChar.isalpha() == True and len(guessedChar) == 1:

            if selected_Word.count(guessedChar) > 0:  ## gussed char found in the word selected from file

                if gussed_word.count(guessedChar) > 0:  ## gussed word alredy have the char user entered
                    print("you already entered this character")
                else:
                    positions=[pos for pos, char in enumerate(selected_Word) if char == guessedChar] ## find all positions of gussed char
                    rightAnswer += len(positions)  ## increase right answer with th size of positions found
                    gussed_word = list(gussed_word)  ## converte gussed word to list
                    for i in range(0, len(positions)):
                        gussed_word[positions[i]] = selected_Word[positions[i]]
                    gussed_word = ''.join(gussed_word)

            else:
                guessNumber -= 1
                wrongGuess += guessedChar
                print("this character is not presented in the word")
        else:
            print("please only letters")

        print("chances remaining : "+str(guessNumber))
        print("missed Letters/Digits : "+wrongGuess)
        print(gussed_word)

    if guessNumber == 0:
        print("Game Over .. the right word is : "+selected_Word)
    else:
        print("You are a winner .. ")

    print("Play Again [Y,N] ?")
    choose = input("enter choice : ")
    if choose == "N" or choose == "n":
        break

